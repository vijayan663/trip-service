package com.onugo.controller;

import com.onugo.CommonTestSetup;
import com.onugo.TestDataUtil;
import com.onugo.constants.ErrorCode;
import com.onugo.model.ErrorResponse;
import com.onugo.model.document.Admin;
import com.onugo.model.document.Customer;
import com.onugo.model.document.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class UserControllerIT extends CommonTestSetup {
    @LocalServerPort
    private int port;
    private String host = "http://localhost:";
    private String ADMIN_END_POINT = "/admin";
    private String CUSTOMER_END_POINT = "/customer";
    private String EMPLOYEE_END_POINT = "/employee";
    private String EMPLOYEES_END_POINT = "/employees";

    private String getApiUrl(String endpoint) {
        return String.format("%s%s%s", host, port, endpoint);
    }

    private String getApiUrl(String endpoint, String value) {
        return String.format("%s%s%s%s", host, port, endpoint, "/"+value);
    }



    @Test
    public void shouldCreateAndFetchAdminSuccessfully(){
        final Admin admin = TestDataUtil.createAdmin();
        final HttpEntity<Admin> entity = new HttpEntity<>(admin);
        final ResponseEntity<Admin> result = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.POST, entity, Admin.class);
        Admin resultAdmin = result.getBody();
        Assert.assertNotNull(resultAdmin.getId());
        Assert.assertEquals(resultAdmin.getFirstName(), admin.getFirstName());
        Assert.assertEquals(resultAdmin.getLastName(), admin.getLastName());

        final ResponseEntity<Admin> getResult = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT, resultAdmin.getId()), HttpMethod.GET, null, Admin.class);
        final Admin admin1 = getResult.getBody();
        Assert.assertNotNull(admin1.getId());
        Assert.assertEquals(admin1.getFirstName(), admin.getFirstName());
        Assert.assertEquals(admin1.getLastName(), admin.getLastName());
    }

    @Test
    public void shouldThrowExceptionAdminEmailAlreadyExist(){
        Admin admin = TestDataUtil.createAdmin();
        final HttpEntity<Admin> entity = new HttpEntity<>(admin);
        final ResponseEntity<Admin> result = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.POST, entity, Admin.class);
        Admin resultAdmin = result.getBody();
        Assert.assertNotNull(resultAdmin.getId());

        Admin admin2 = TestDataUtil.createAdmin(admin.getContact().getEmail());
        final HttpEntity<Admin> entity2 = new HttpEntity<>(admin2);
        final ResponseEntity<ErrorResponse> errorResult = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.POST, entity2, ErrorResponse.class);
        Assert.assertEquals(errorResult.getBody().getErrorMessage(), ErrorCode.E0018.getErrorMessage());
    }

    @Test
    public void shouldUpdateAdminSuccessfully(){
        final Admin admin = TestDataUtil.createAdmin();
        HttpEntity<Admin> entity = new HttpEntity<>(admin);
        ResponseEntity<Admin> result = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.POST, entity, Admin.class);
        Admin resultAdmin = result.getBody();
        Assert.assertNotNull(resultAdmin.getId());

        TestDataUtil.updateAdmin(resultAdmin,false);
        HttpEntity<Admin> updateEntity = new HttpEntity<>(resultAdmin);
        ResponseEntity<Boolean> updateResult = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.PUT, updateEntity, Boolean.class);
        Assert.assertTrue(updateResult.getBody());
    }

    @Test
    public void shouldThrowExceptionForUpdateAdminIdNull(){
        HttpEntity<Admin> updateEntity = new HttpEntity<>(TestDataUtil.createAdmin());
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0023.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForAdminIdNotExist(){
        Admin admin = TestDataUtil.createAdmin();
        admin.setId("0");
        HttpEntity<Admin> updateEntity = new HttpEntity<>(admin);
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0024.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForUpdateAdminAlreadyExist(){
        final Admin admin1 = TestDataUtil.createAdmin();
        final Admin admin2 = TestDataUtil.createAdmin();
        ResponseEntity<Admin> result1 = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.POST, new HttpEntity<>(admin1), Admin.class);
        ResponseEntity<Admin> result2 = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.POST, new HttpEntity<>(admin2), Admin.class);
        Assert.assertNotNull(result1.getBody().getId());

        final Admin resultAdmin = result2.getBody();
        Assert.assertNotNull(resultAdmin.getId());

        resultAdmin.getContact().setEmail(admin1.getContact().getEmail());
        HttpEntity<Admin> updateEntity = new HttpEntity<>(resultAdmin);
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(ADMIN_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0018.getErrorMessage());
    }



    @Test
    public void shouldCreateAndFetchEmployeeSuccessfully(){
        final Employee employee = TestDataUtil.createEmployee();
        final HttpEntity<Employee> entity = new HttpEntity<>(employee);
        final ResponseEntity<Employee> result = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, entity, Employee.class);
        Employee resultEmployee = result.getBody();
        Assert.assertNotNull(resultEmployee.getId());
        Assert.assertEquals(resultEmployee.getFirstName(), employee.getFirstName());
        Assert.assertEquals(resultEmployee.getLastName(), employee.getLastName());

        final ResponseEntity<Employee> getResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT, resultEmployee.getId()), HttpMethod.GET, null, Employee.class);
        final Employee employee1 = getResult.getBody();
        Assert.assertNotNull(employee1.getId());
        Assert.assertEquals(employee1.getFirstName(), employee.getFirstName());
        Assert.assertEquals(employee1.getLastName(), employee.getLastName());
    }

    @Test
    public void shouldCreateAndFetchAllEmployeeSuccessfully(){
        final HttpEntity<Employee> entity = new HttpEntity<>(TestDataUtil.createEmployee());
        final ResponseEntity<Employee> result = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, entity, Employee.class);
        Employee resultEmployee = result.getBody();
        Assert.assertNotNull(resultEmployee.getId());

        final HttpEntity<Employee> entity2 = new HttpEntity<>(TestDataUtil.createEmployee());
        final ResponseEntity<Employee> result2 = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, entity2, Employee.class);
        Employee resultEmployee2 = result2.getBody();
        Assert.assertNotNull(resultEmployee2.getId());


        final ResponseEntity<List<Employee>> getResult = testRestTemplate.exchange(getApiUrl(EMPLOYEES_END_POINT), HttpMethod.GET, null, new ParameterizedTypeReference<List<Employee>>(){});
        Assert.assertTrue(getResult.getBody().size()>2);
    }

    @Test
    public void shouldThrowExceptionEmployeeEmailAlreadyExist(){
        Employee employee = TestDataUtil.createEmployee();
        final HttpEntity<Employee> entity = new HttpEntity<>(employee);
        final ResponseEntity<Employee> result = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, entity, Employee.class);
        Employee resultEmployee = result.getBody();
        Assert.assertNotNull(resultEmployee.getId());

        Employee employee2 = TestDataUtil.createEmployee(employee.getContact().getEmail());
        final HttpEntity<Employee> entity2 = new HttpEntity<>(employee2);
        final ResponseEntity<ErrorResponse> errorResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, entity2, ErrorResponse.class);
        Assert.assertEquals(errorResult.getBody().getErrorMessage(), ErrorCode.E0018.getErrorMessage());
    }

    @Test
    public void shouldUpdateEmployeeSuccessfully(){
        final Employee employee = TestDataUtil.createEmployee();
        HttpEntity<Employee> entity = new HttpEntity<>(employee);
        ResponseEntity<Employee> result = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, entity, Employee.class);
        Employee resultEmployee = result.getBody();
        Assert.assertNotNull(resultEmployee.getId());

        TestDataUtil.updateEmployee(resultEmployee,false);
        HttpEntity<Employee> updateEntity = new HttpEntity<>(resultEmployee);
        ResponseEntity<Boolean> updateResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.PUT, updateEntity, Boolean.class);
        Assert.assertTrue(updateResult.getBody());
    }

    @Test
    public void shouldThrowExceptionForUpdateEmployeeIdNull(){
        HttpEntity<Employee> updateEntity = new HttpEntity<>(TestDataUtil.createEmployee());
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0021.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForEmployeeIdNotExist(){
        Employee employee = TestDataUtil.createEmployee();
        employee.setId("0");
        HttpEntity<Employee> updateEntity = new HttpEntity<>(employee);
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0022.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForUpdateEmployeeAlreadyExist(){
        final Employee employee1 = TestDataUtil.createEmployee();
        final Employee employee2 = TestDataUtil.createEmployee();
        ResponseEntity<Employee> result1 = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, new HttpEntity<>(employee1), Employee.class);
        ResponseEntity<Employee> result2 = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, new HttpEntity<>(employee2), Employee.class);
        Assert.assertNotNull(result1.getBody().getId());

        final Employee resultCustomer = result2.getBody();
        Assert.assertNotNull(resultCustomer.getId());

        resultCustomer.getContact().setEmail(employee1.getContact().getEmail());
        HttpEntity<Employee> updateEntity = new HttpEntity<>(resultCustomer);
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0018.getErrorMessage());
    }



    @Test
    public void shouldCreateAndFetchCustomerSuccessfully(){
        final Customer customer = TestDataUtil.createCustomer();
        final HttpEntity<Customer> entity = new HttpEntity<>(customer);
        final ResponseEntity<Customer> result = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.POST, entity, Customer.class);
        Customer resultCustomer = result.getBody();
        Assert.assertNotNull(resultCustomer.getId());
        Assert.assertEquals(resultCustomer.getFirstName(), customer.getFirstName());
        Assert.assertEquals(resultCustomer.getLastName(), customer.getLastName());

        final ResponseEntity<Customer> getResult = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT, resultCustomer.getId()), HttpMethod.GET, null, Customer.class);
        final Customer customer1 = getResult.getBody();
        Assert.assertNotNull(customer1.getId());
        Assert.assertEquals(customer1.getFirstName(), customer.getFirstName());
        Assert.assertEquals(customer1.getLastName(), customer.getLastName());
    }

    @Test
    public void shouldThrowExceptionCustomerEmailAlreadyExist(){
        Customer customer = TestDataUtil.createCustomer();
        final HttpEntity<Customer> entity = new HttpEntity<>(customer);
        final ResponseEntity<Customer> result = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.POST, entity, Customer.class);
        Customer resultCustomer = result.getBody();
        Assert.assertNotNull(resultCustomer.getId());

        Customer customer2 = TestDataUtil.createCustomer(customer.getContact().getEmail());
        final HttpEntity<Customer> entity2 = new HttpEntity<>(customer2);
        final ResponseEntity<ErrorResponse> errorResult = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.POST, entity2, ErrorResponse.class);
        Assert.assertEquals(errorResult.getBody().getErrorMessage(), ErrorCode.E0018.getErrorMessage());
    }

    @Test
    public void shouldUpdateCustomerSuccessfully(){
        final Customer customer = TestDataUtil.createCustomer();
        HttpEntity<Customer> entity = new HttpEntity<>(customer);
        ResponseEntity<Customer> result = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.POST, entity, Customer.class);
        Customer resultCustomer = result.getBody();
        Assert.assertNotNull(resultCustomer.getId());

        TestDataUtil.updateCustomer(resultCustomer,false);
        HttpEntity<Customer> updateEntity = new HttpEntity<>(resultCustomer);
        ResponseEntity<Boolean> updateResult = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.PUT, updateEntity, Boolean.class);
        Assert.assertTrue(updateResult.getBody());
    }

    @Test
    public void shouldThrowExceptionForUpdateCustomerIdNull(){
        HttpEntity<Customer> updateEntity = new HttpEntity<>(TestDataUtil.createCustomer());
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0019.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForCustomerIdNotExist(){
        Customer customer = TestDataUtil.createCustomer();
        customer.setId("0");
        HttpEntity<Customer> updateEntity = new HttpEntity<>(customer);
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0020.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForUpdateCustomerAlreadyExist(){
        final Customer customer1 = TestDataUtil.createCustomer();
        final Customer customer2 = TestDataUtil.createCustomer();
        ResponseEntity<Customer> result1 = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.POST, new HttpEntity<>(customer1), Customer.class);
        ResponseEntity<Customer> result2 = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.POST, new HttpEntity<>(customer2), Customer.class);
        Assert.assertNotNull(result1.getBody().getId());

        final Customer resultCustomer = result2.getBody();
        Assert.assertNotNull(resultCustomer.getId());

        resultCustomer.getContact().setEmail(customer1.getContact().getEmail());
        HttpEntity<Customer> updateEntity = new HttpEntity<>(resultCustomer);
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0018.getErrorMessage());
    }
}
