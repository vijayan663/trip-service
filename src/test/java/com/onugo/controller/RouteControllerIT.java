package com.onugo.controller;

import com.onugo.CommonTestSetup;
import com.onugo.TestDataUtil;
import com.onugo.constants.ErrorCode;
import com.onugo.model.ErrorResponse;
import com.onugo.model.document.Route;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class RouteControllerIT extends CommonTestSetup {
    @LocalServerPort
    private int port;
    private String host = "http://localhost:";
    private String ROUTE_END_POINT = "/route";
    private String ROUTES_END_POINT = "/routes";
    private String ROUTE_BY_ID_END_POINT = "/route/id/";
    private String ROUTE_BY_ADDRESS_END_POINT = "/route/address/";

    private String getApiUrl(String endpoint) {
        return String.format("%s%s%s", host, port, endpoint);
    }

    private String getApiUrl(String endpoint, String value) {
        return String.format("%s%s%s%s", host, port, endpoint, value);
    }

    @Test
    public void shouldCreateAndFetchAndDeleteRouteSuccessfully(){
        final String address = TestDataUtil.getRandomRouteAddress();
        final HttpEntity<Route> entity = new HttpEntity<>(TestDataUtil.createRoute(address));
        final ResponseEntity<Route> result = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.POST, entity, Route.class);
        final Route route = result.getBody();
        Assert.assertNotNull(route.getId());
        Assert.assertEquals(route.getRouteAddress(), address.toUpperCase());

        final ResponseEntity<Route> routeResponseEntity = testRestTemplate.exchange(getApiUrl(ROUTE_BY_ID_END_POINT, route.getId()), HttpMethod.GET, null, Route.class);
        final Route responseRoute = routeResponseEntity.getBody();
        Assert.assertNotNull(responseRoute.getId());
        Assert.assertEquals(route.getRouteAddress(), address.toUpperCase());

        final ResponseEntity<Boolean> routeDeleteResponseEntity = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT, "/"+route.getId()), HttpMethod.DELETE, null, Boolean.class);
        Assert.assertTrue(routeDeleteResponseEntity.getBody());
    }

    @Test
    public void shouldThrowExceptionForCreateRouteAlreadyExist(){
        final String address = TestDataUtil.getRandomRouteAddress();
        final HttpEntity<Route> entity = new HttpEntity<>(TestDataUtil.createRoute(address));
        final ResponseEntity<Route> result = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.POST, entity, Route.class);
        final Route route = result.getBody();
        Assert.assertNotNull(route.getId());
        Assert.assertEquals(route.getRouteAddress(), address.toUpperCase());

        final ResponseEntity<ErrorResponse> errorResult = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.POST, entity, ErrorResponse.class);
        Assert.assertEquals(errorResult.getBody().getErrorMessage(), ErrorCode.E0013.getErrorMessage());
    }

    @Test
    public void shouldUpdateRouteSuccessfully(){
        final String address = TestDataUtil.getRandomRouteAddress();
        final String newAddress = TestDataUtil.getRandomRouteAddress();
        HttpEntity<Route> entity = new HttpEntity<>(TestDataUtil.createRoute(address));
        ResponseEntity<Route> result = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.POST, entity, Route.class);
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertEquals(result.getBody().getRouteAddress(), address.toUpperCase());

        final ResponseEntity<Route> routeResponseEntity = testRestTemplate.exchange(getApiUrl(ROUTE_BY_ADDRESS_END_POINT, address), HttpMethod.GET, null, Route.class);
        final Route route = routeResponseEntity.getBody();
        Assert.assertNotNull(route);


        route.setRouteAddress(newAddress);
        HttpEntity<Route> updateEntity = new HttpEntity<>(route);
        ResponseEntity<Boolean> updateResult = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.PUT, updateEntity, Boolean.class);
        Assert.assertTrue(updateResult.getBody());
    }

    @Test
    public void shouldThrowExceptionForUpdateRouteIdNull(){
        HttpEntity<Route> updateEntity = new HttpEntity<>(TestDataUtil.createRoute("Test"));
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0017.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForUpdateRouteIdNotExist(){
        Route route = TestDataUtil.createRoute("Test");
        route.setId("ffffffff-ffff-ffff-ffff-ffffffffffff");
        HttpEntity<Route> updateEntity = new HttpEntity<>(route);
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0014.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForUpdateRouteAlreadyExist(){
        final String address1 = TestDataUtil.getRandomRouteAddress();
        final String address2 = TestDataUtil.getRandomRouteAddress();
        ResponseEntity<Route> result1 = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.POST, new HttpEntity<>(TestDataUtil.createRoute(address1)), Route.class);
        ResponseEntity<Route> result2 = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.POST, new HttpEntity<>(TestDataUtil.createRoute(address2)), Route.class);
        Assert.assertNotNull(result1.getBody().getId());
        Assert.assertEquals(result1.getBody().getRouteAddress(), address1.toUpperCase());
        final Route route = result2.getBody();
        Assert.assertNotNull(route.getId());
        Assert.assertEquals(route.getRouteAddress(), address2.toUpperCase());

        route.setRouteAddress(address1);
        HttpEntity<Route> updateEntity = new HttpEntity<>(route);
        ResponseEntity<ErrorResponse> updateResult = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.PUT, updateEntity, ErrorResponse.class);
        Assert.assertEquals(updateResult.getBody().getErrorMessage(),ErrorCode.E0015.getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionForDeleteRouteIdNotExist(){
        final ResponseEntity<ErrorResponse> routeDeleteResponseEntity = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT, "/ffffffff-ffff-ffff-ffff-ffffffffffff"), HttpMethod.DELETE, null, ErrorResponse.class);
        Assert.assertEquals(routeDeleteResponseEntity.getBody().getErrorMessage(), ErrorCode.E0016.getErrorMessage());
    }

    @Test
    public void shouldCreateAndFetchMultipleRoutesSuccessfully(){
        List<Route> routes = new ArrayList<>();
        IntStream stream = IntStream.range(1,6);
        stream.forEach(s-> routes.add(TestDataUtil.createRoute()));

        HttpEntity<List<Route>> entity = new HttpEntity<>(routes);
        ResponseEntity<List<Route>> result = testRestTemplate.exchange(getApiUrl(ROUTES_END_POINT),  HttpMethod.POST, entity, new ParameterizedTypeReference<List<Route>>(){});
        Assert.assertTrue(result.getBody().size()>0);

        ResponseEntity<List<Route>> getResult = testRestTemplate.exchange(getApiUrl(ROUTES_END_POINT),  HttpMethod.GET, entity,  new ParameterizedTypeReference<List<Route>>(){});
        Assert.assertTrue(result.getBody().size()>0);
    }

    @Test
    public void shouldThrowExceptionForCreateRouteWithRouteAlreadyExists(){
        List<Route> routes = new ArrayList<>();
        IntStream stream = IntStream.range(1,2);
        stream.forEach(s-> routes.add(TestDataUtil.createRoute()));

        HttpEntity<List<Route>> entity = new HttpEntity<>(routes);
        ResponseEntity<List<Route>> result = testRestTemplate.exchange(getApiUrl(ROUTES_END_POINT),  HttpMethod.POST, entity, new ParameterizedTypeReference<List<Route>>(){});
        Assert.assertTrue(result.getBody().size()>0);

        ResponseEntity<ErrorResponse> errorResult = testRestTemplate.exchange(getApiUrl(ROUTES_END_POINT),  HttpMethod.POST, entity, ErrorResponse.class);
        Assert.assertEquals(errorResult.getBody().getErrorMessage(), ErrorCode.E0013.getErrorMessage());
    }
}
