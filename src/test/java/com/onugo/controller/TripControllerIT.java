package com.onugo.controller;

import com.onugo.CommonTestSetup;
import com.onugo.TestDataUtil;
import com.onugo.constants.ErrorCode;
import com.onugo.model.ErrorResponse;
import com.onugo.model.document.Customer;
import com.onugo.model.document.Employee;
import com.onugo.model.document.Location;
import com.onugo.model.document.Route;
import com.onugo.model.document.Trip;
import com.onugo.model.document.TripActive;
import com.onugo.model.document.TripHistory;
import com.onugo.model.document.TripPending;
import com.onugo.model.document.TripStatus;
import com.onugo.model.request.AcceptTripRequest;
import com.onugo.model.request.BookTripRequest;
import com.onugo.model.request.CancelTripRequest;
import com.onugo.model.request.UpdateTripStatusForEmployeeRequest;
import com.onugo.model.response.BookTripResponse;
import com.onugo.model.response.CustomerBookedTripResponse;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;

public class TripControllerIT extends CommonTestSetup {
    @LocalServerPort
    private int port;
    private String host = "http://localhost:";
    private String BOOK_TRIP_END_POINT = "/bookTrip";
    private String CANCEL_TRIP_END_POINT = "/cancelTrip";
    private String ACCEPT_TRIP_END_POINT = "/acceptTrip";
    private String GET_CUSTOMER_BOOKED_END_POINT = "/getCustomerBookedTrip/";
    private String GET_ALL_PENDING_TRIPS_END_POINT = "/getAllPendingTrips";
    private String GET_CUSTOMER_TRIPS_HISTORY_END_POINT = "/getCustomerTripsHistory/";
    private String GET_EMPLPOYEE_TRIPS_HISTORY_END_POINT = "/getEmployeeTripsHistory/";
    private String UPDATE_EMPLPOYEE_TRIP_STATUS_END_POINT = "/updateTripStatusForEmployee";
    private String GET_ALL_ACTIVE_TRIPS_END_POINT = "/getAllActiveTrips/";
    private String TRIP_HISTORY_END_POINT = "/tripHistory/";
    private String TRIP_ACTIVE_END_POINT = "/tripActive/";
    private String TRIP_PENDING_END_POINT = "/tripPending/";
    private String ROUTE_END_POINT = "/route";
    private String CUSTOMER_END_POINT = "/customer";
    private String EMPLOYEE_END_POINT = "/employee";
    private String CUSTOMER_END_POINT_WITH_VALUE = "/customer/";


    private String getApiUrl(String endpoint) {
        return String.format("%s%s%s", host, port, endpoint);
    }

    private String getApiUrl(String endpoint, String value) {
        return String.format("%s%s%s%s", host, port, endpoint, value);
    }

    private Route createRoute(){
        final HttpEntity<Route> entity = new HttpEntity<>(TestDataUtil.createRoute());
        final ResponseEntity<Route> result = testRestTemplate.exchange(getApiUrl(ROUTE_END_POINT), HttpMethod.POST, entity, Route.class);
        return result.getBody();
    }

    private Location createLocation(){
        Location location = new Location();
        Route route = createRoute();
        location.setAddress(route.getRouteAddress());
        return location;
    }

    private Location createLocation(String address){
        Location location = new Location();
        location.setAddress(address);
        return location;
    }
    private Customer createCustomer(){
        final Customer customer = TestDataUtil.createCustomer();
        final HttpEntity<Customer> entity = new HttpEntity<>(customer);
        final ResponseEntity<Customer> result = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT), HttpMethod.POST, entity, Customer.class);
        return result.getBody();
    }
    private Trip createTrip(){
        Trip trip = new Trip();
        trip.setCustomer(createCustomer());
        trip.setTripStatus(TripStatus.PENDING);
        trip.setStartLocation(createLocation());
        trip.setEndLocation(createLocation());
        trip.setStartTime(new Date());
        return trip;
    }

    private BookTripRequest createBookTripRequest(){
        final BookTripRequest bookTripRequest = new BookTripRequest();
        bookTripRequest.setCustomer(createCustomer());
        bookTripRequest.setStartLocation(createLocation());
        bookTripRequest.setEndLocation(createLocation());
        bookTripRequest.setStartTime(DateUtils.addMinutes(new Date(), 60));
        return bookTripRequest;
    }
    private BookTripRequest createBookTripRequest(Date startTime){
        final BookTripRequest bookTripRequest = new BookTripRequest();
        bookTripRequest.setCustomer(createCustomer());
        bookTripRequest.setStartLocation(createLocation());
        bookTripRequest.setEndLocation(createLocation());
        bookTripRequest.setStartTime(startTime);
        return bookTripRequest;
    }

    private BookTripRequest createBookTripRequest(Customer customer){
        final BookTripRequest bookTripRequest = new BookTripRequest();
        bookTripRequest.setCustomer(customer);
        bookTripRequest.setStartLocation(createLocation());
        bookTripRequest.setEndLocation(createLocation());
        bookTripRequest.setStartTime(DateUtils.addMinutes(new Date(), 60));
        return bookTripRequest;
    }

    private BookTripRequest createBookTripRequest(Location startLocation, Location endLocation){
        final BookTripRequest bookTripRequest = new BookTripRequest();
        bookTripRequest.setCustomer(createCustomer());
        bookTripRequest.setStartLocation(startLocation);
        bookTripRequest.setEndLocation(endLocation);
        bookTripRequest.setStartTime(DateUtils.addMinutes(new Date(), 60));
        return bookTripRequest;
    }

    @Test
    public void shouldBookAndFetchTripSuccessfully(){
        final BookTripRequest bookTripRequest = createBookTripRequest();
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<BookTripResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, BookTripResponse.class);
        final BookTripResponse bookTripResponse = result.getBody();
        TripPending tripPending = bookTripResponse.getTripPending();
        Assert.assertNotNull(tripPending.getTripId());
        Assert.assertNotNull(tripPending.getCustomer().getBookedTripId());

        final ResponseEntity<CustomerBookedTripResponse> customerBookedTripResponseResponseEntity = testRestTemplate.exchange(getApiUrl(GET_CUSTOMER_BOOKED_END_POINT,tripPending.getTripId()), HttpMethod.GET, null, CustomerBookedTripResponse.class);
        CustomerBookedTripResponse customerBookedTripResponse = customerBookedTripResponseResponseEntity.getBody();
        Assert.assertEquals(customerBookedTripResponse.getTripPending().getTripId(),tripPending.getTripId());

        final ResponseEntity<Customer> getResult = testRestTemplate.exchange(getApiUrl(CUSTOMER_END_POINT_WITH_VALUE, bookTripResponse.getTripPending().getCustomer().getId()), HttpMethod.GET, null, Customer.class);
        final Customer customer1 = getResult.getBody();
        Assert.assertEquals(tripPending.getTripId(), customer1.getBookedTripId());

        final ResponseEntity<List<TripPending>> getPendingTripsResult = testRestTemplate.exchange(getApiUrl(GET_ALL_PENDING_TRIPS_END_POINT), HttpMethod.GET, null, new ParameterizedTypeReference<List<TripPending>>(){});
        Assert.assertTrue(getPendingTripsResult.getBody().size()>0);
    }

    @Test
    public void shouldBookAndCancelTripSuccessfully(){
        final BookTripRequest bookTripRequest = createBookTripRequest();
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<BookTripResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, BookTripResponse.class);
        final BookTripResponse bookTripResponse = result.getBody();
        TripPending tripPending = bookTripResponse.getTripPending();
        final String tripid = tripPending.getTripId();
        Assert.assertNotNull(tripid);
        Assert.assertNotNull(tripPending.getCustomer().getBookedTripId());

        final CancelTripRequest cancelTripRequest = new CancelTripRequest();
        cancelTripRequest.setTripId(tripid);
        final HttpEntity<CancelTripRequest> cancelTripRequestEntity = new HttpEntity<>(cancelTripRequest);
        final ResponseEntity<Boolean> cancelResult = testRestTemplate.exchange(getApiUrl(CANCEL_TRIP_END_POINT), HttpMethod.POST, cancelTripRequestEntity, Boolean.class);
        Assert.assertTrue(cancelResult.getBody());

        final ResponseEntity<TripPending> result2 = testRestTemplate.exchange(getApiUrl(TRIP_PENDING_END_POINT,tripid), HttpMethod.GET, null, TripPending.class);
        Assert.assertNull(result2.getBody());

        final ResponseEntity<TripHistory> result3 = testRestTemplate.exchange(getApiUrl(TRIP_HISTORY_END_POINT,tripid), HttpMethod.GET, null, TripHistory.class);
        Assert.assertEquals(tripid, result3.getBody().getTripId());


        final ResponseEntity<List<TripHistory>> result4 = testRestTemplate.exchange(getApiUrl(GET_CUSTOMER_TRIPS_HISTORY_END_POINT,result3.getBody().getCustomer().getId()), HttpMethod.GET, null, new ParameterizedTypeReference<List<TripHistory>>(){});
        Assert.assertEquals(1, result4.getBody().size());
        Assert.assertEquals(tripid, result4.getBody().get(0).getTripId());
    }

    @Test
    public void shouldBookAndAcceptTripSuccessfully(){
        final BookTripRequest bookTripRequest = createBookTripRequest();
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<BookTripResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, BookTripResponse.class);
        final BookTripResponse bookTripResponse = result.getBody();
        TripPending tripPending = bookTripResponse.getTripPending();
        final String tripid = tripPending.getTripId();
        Assert.assertNotNull(tripid);
        Assert.assertNotNull(tripPending.getCustomer().getBookedTripId());

        final Employee employee = TestDataUtil.createEmployee();
        final HttpEntity<Employee> employeeEntity = new HttpEntity<>(employee);
        final ResponseEntity<Employee> employeeResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, employeeEntity, Employee.class);
        Employee resultEmployee = employeeResult.getBody();
        Assert.assertNotNull(resultEmployee.getId());
        Assert.assertEquals(resultEmployee.getFirstName(), employee.getFirstName());
        Assert.assertEquals(resultEmployee.getLastName(), employee.getLastName());

        final AcceptTripRequest acceptTripRequest = new AcceptTripRequest();
        acceptTripRequest.setEmployee(resultEmployee);
        acceptTripRequest.setAcceptedBy(resultEmployee.getId());
        acceptTripRequest.setTripId(tripid);
        final HttpEntity<AcceptTripRequest> acceptTripRequestEntity = new HttpEntity<>(acceptTripRequest);
        final ResponseEntity<Boolean> acceptResult = testRestTemplate.exchange(getApiUrl(ACCEPT_TRIP_END_POINT), HttpMethod.POST, acceptTripRequestEntity, Boolean.class);
        Assert.assertTrue(acceptResult.getBody());

        final ResponseEntity<TripPending> result2 = testRestTemplate.exchange(getApiUrl(TRIP_PENDING_END_POINT,tripid), HttpMethod.GET, null, TripPending.class);
        Assert.assertNull(result2.getBody());

        final ResponseEntity<TripActive> result3 = testRestTemplate.exchange(getApiUrl(TRIP_ACTIVE_END_POINT,tripid), HttpMethod.GET, null, TripActive.class);
        Assert.assertEquals(tripid, result3.getBody().getTripId());

        final ResponseEntity<List<TripActive> > result4 = testRestTemplate.exchange(getApiUrl(GET_ALL_ACTIVE_TRIPS_END_POINT,TripStatus.SCHEDULED.name()), HttpMethod.GET, null, new ParameterizedTypeReference<List<TripActive>>(){});
        Assert.assertTrue(result4.getBody().size()>0);
    }

    @Test
    public void shouldBookAndAcceptAndCancelTripSuccessfully(){
        final BookTripRequest bookTripRequest = createBookTripRequest();
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<BookTripResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, BookTripResponse.class);
        final BookTripResponse bookTripResponse = result.getBody();
        TripPending tripPending = bookTripResponse.getTripPending();
        final String tripid = tripPending.getTripId();
        Assert.assertNotNull(tripid);
        Assert.assertNotNull(tripPending.getCustomer().getBookedTripId());

        final Employee employee = TestDataUtil.createEmployee();
        final HttpEntity<Employee> employeeEntity = new HttpEntity<>(employee);
        final ResponseEntity<Employee> employeeResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, employeeEntity, Employee.class);
        Employee resultEmployee = employeeResult.getBody();
        Assert.assertNotNull(resultEmployee.getId());
        Assert.assertEquals(resultEmployee.getFirstName(), employee.getFirstName());
        Assert.assertEquals(resultEmployee.getLastName(), employee.getLastName());

        final AcceptTripRequest acceptTripRequest = new AcceptTripRequest();
        acceptTripRequest.setEmployee(resultEmployee);
        acceptTripRequest.setAcceptedBy(resultEmployee.getId());
        acceptTripRequest.setTripId(tripid);
        final HttpEntity<AcceptTripRequest> acceptTripRequestEntity = new HttpEntity<>(acceptTripRequest);
        final ResponseEntity<Boolean> acceptResult = testRestTemplate.exchange(getApiUrl(ACCEPT_TRIP_END_POINT), HttpMethod.POST, acceptTripRequestEntity, Boolean.class);
        Assert.assertTrue(acceptResult.getBody());


        final ResponseEntity<TripPending> result2 = testRestTemplate.exchange(getApiUrl(TRIP_PENDING_END_POINT,tripid), HttpMethod.GET, null, TripPending.class);
        Assert.assertNull(result2.getBody());

        final ResponseEntity<TripActive> result3 = testRestTemplate.exchange(getApiUrl(TRIP_ACTIVE_END_POINT,tripid), HttpMethod.GET, null, TripActive.class);
        Assert.assertEquals(tripid, result3.getBody().getTripId());

        final CancelTripRequest cancelTripRequest = new CancelTripRequest();
        cancelTripRequest.setTripId(tripid);
        final HttpEntity<CancelTripRequest> cancelTripRequestEntity = new HttpEntity<>(cancelTripRequest);
        final ResponseEntity<Boolean> cancelResult = testRestTemplate.exchange(getApiUrl(CANCEL_TRIP_END_POINT), HttpMethod.POST, cancelTripRequestEntity, Boolean.class);
        Assert.assertTrue(cancelResult.getBody());

        final ResponseEntity<TripPending> result4 = testRestTemplate.exchange(getApiUrl(TRIP_PENDING_END_POINT,tripid), HttpMethod.GET, null, TripPending.class);
        Assert.assertNull(result4.getBody());

        final ResponseEntity<TripHistory> result5 = testRestTemplate.exchange(getApiUrl(TRIP_HISTORY_END_POINT,tripid), HttpMethod.GET, null, TripHistory.class);
        Assert.assertEquals(tripid, result5.getBody().getTripId());
    }


    @Test
    public void shouldBookAndAcceptAndCompleteTripSuccessfully(){
        final BookTripRequest bookTripRequest = createBookTripRequest();
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<BookTripResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, BookTripResponse.class);
        final BookTripResponse bookTripResponse = result.getBody();
        TripPending tripPending = bookTripResponse.getTripPending();
        final String tripid = tripPending.getTripId();
        Assert.assertNotNull(tripid);
        Assert.assertNotNull(tripPending.getCustomer().getBookedTripId());

        final Employee employee = TestDataUtil.createEmployee();
        final HttpEntity<Employee> employeeEntity = new HttpEntity<>(employee);
        final ResponseEntity<Employee> employeeResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, employeeEntity, Employee.class);
        Employee resultEmployee = employeeResult.getBody();
        Assert.assertNotNull(resultEmployee.getId());
        Assert.assertEquals(resultEmployee.getFirstName(), employee.getFirstName());
        Assert.assertEquals(resultEmployee.getLastName(), employee.getLastName());

        final AcceptTripRequest acceptTripRequest = new AcceptTripRequest();
        acceptTripRequest.setEmployee(resultEmployee);
        acceptTripRequest.setAcceptedBy(resultEmployee.getId());
        acceptTripRequest.setTripId(tripid);
        final HttpEntity<AcceptTripRequest> acceptTripRequestEntity = new HttpEntity<>(acceptTripRequest);
        final ResponseEntity<Boolean> acceptResult = testRestTemplate.exchange(getApiUrl(ACCEPT_TRIP_END_POINT), HttpMethod.POST, acceptTripRequestEntity, Boolean.class);
        Assert.assertTrue(acceptResult.getBody());

        final UpdateTripStatusForEmployeeRequest updateTripStatusForEmployeeRequest = createUpdateTripStatusForEmployeeRequest(tripid, TripStatus.IN_PROGRESS, resultEmployee.getId());
        final HttpEntity<UpdateTripStatusForEmployeeRequest> updateTripStatusForEmployeeRequestEntity = new HttpEntity<>(updateTripStatusForEmployeeRequest);
        final ResponseEntity<Boolean> result2 = testRestTemplate.exchange(getApiUrl(UPDATE_EMPLPOYEE_TRIP_STATUS_END_POINT), HttpMethod.POST, updateTripStatusForEmployeeRequestEntity, Boolean.class);
        Assert.assertTrue(result2.getBody());

        final ResponseEntity<List<TripActive> > getAllActiveTripsResult = testRestTemplate.exchange(getApiUrl(GET_ALL_ACTIVE_TRIPS_END_POINT,TripStatus.IN_PROGRESS.name()), HttpMethod.GET, null, new ParameterizedTypeReference<List<TripActive>>(){});
        Assert.assertTrue(getAllActiveTripsResult.getBody().size()>0);

        final UpdateTripStatusForEmployeeRequest updateTripStatusForEmployeeRequest2 = createUpdateTripStatusForEmployeeRequest(tripid, TripStatus.COMPLETED, resultEmployee.getId());
        final HttpEntity<UpdateTripStatusForEmployeeRequest> updateTripStatusForEmployeeRequestEntity2 = new HttpEntity<>(updateTripStatusForEmployeeRequest2);
        final ResponseEntity<Boolean> result3 = testRestTemplate.exchange(getApiUrl(UPDATE_EMPLPOYEE_TRIP_STATUS_END_POINT), HttpMethod.POST, updateTripStatusForEmployeeRequestEntity2, Boolean.class);
        Assert.assertTrue(result3.getBody());

        final ResponseEntity<TripPending> result4 = testRestTemplate.exchange(getApiUrl(TRIP_ACTIVE_END_POINT,tripid), HttpMethod.GET, null, TripPending.class);
        Assert.assertNull(result4.getBody());

        final ResponseEntity<TripHistory> result5 = testRestTemplate.exchange(getApiUrl(TRIP_HISTORY_END_POINT,tripid), HttpMethod.GET, null, TripHistory.class);
        Assert.assertEquals(tripid, result5.getBody().getTripId());

        final ResponseEntity<List<TripHistory> > result6 = testRestTemplate.exchange(getApiUrl(GET_EMPLPOYEE_TRIPS_HISTORY_END_POINT,resultEmployee.getId()), HttpMethod.GET, null, new ParameterizedTypeReference<List<TripHistory>>(){});
        Assert.assertTrue(result6.getBody().size()>0);
    }

    private UpdateTripStatusForEmployeeRequest createUpdateTripStatusForEmployeeRequest(String tripId, TripStatus tripStatus, String employeeId){
        UpdateTripStatusForEmployeeRequest updateTripStatusForEmployeeRequest = new UpdateTripStatusForEmployeeRequest();
        updateTripStatusForEmployeeRequest.setTripId(tripId);
        updateTripStatusForEmployeeRequest.setTripStatus(tripStatus);
        updateTripStatusForEmployeeRequest.setUpdateBy(employeeId);
        return updateTripStatusForEmployeeRequest;
    }

    @Test
    public void shouldThrowExceptionCancelTripWithTripDoesNotExist(){
        final CancelTripRequest cancelTripRequest = new CancelTripRequest();
        cancelTripRequest.setTripId("0");
        final HttpEntity<CancelTripRequest> cancelTripRequestEntity = new HttpEntity<>(cancelTripRequest);
        final ResponseEntity<ErrorResponse> cancelResult = testRestTemplate.exchange(getApiUrl(CANCEL_TRIP_END_POINT), HttpMethod.POST, cancelTripRequestEntity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0007.getErrorMessage(), cancelResult.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionAcceptTripWithTripDoesNotExist(){
        final Employee employee = TestDataUtil.createEmployee();
        final HttpEntity<Employee> employeeEntity = new HttpEntity<>(employee);
        final ResponseEntity<Employee> employeeResult = testRestTemplate.exchange(getApiUrl(EMPLOYEE_END_POINT), HttpMethod.POST, employeeEntity, Employee.class);
        Employee resultEmployee = employeeResult.getBody();
        Assert.assertNotNull(resultEmployee.getId());
        Assert.assertEquals(resultEmployee.getFirstName(), employee.getFirstName());
        Assert.assertEquals(resultEmployee.getLastName(), employee.getLastName());

        final AcceptTripRequest acceptTripRequest = new AcceptTripRequest();
        acceptTripRequest.setEmployee(resultEmployee);
        acceptTripRequest.setAcceptedBy(resultEmployee.getId());
        acceptTripRequest.setTripId("0");
        final HttpEntity<AcceptTripRequest> acceptTripRequestEntity = new HttpEntity<>(acceptTripRequest);
        final ResponseEntity<ErrorResponse> acceptResult = testRestTemplate.exchange(getApiUrl(ACCEPT_TRIP_END_POINT), HttpMethod.POST, acceptTripRequestEntity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0033.getErrorMessage(), acceptResult.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionBookTripWithTimeLessThanMinimumTime(){
        final BookTripRequest bookTripRequest = createBookTripRequest(new Date());
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0025.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionBookTripWithTimeGreaterThanMaximumTime(){
        final BookTripRequest bookTripRequest = createBookTripRequest(DateUtils.addMinutes(new Date(),350));
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0029.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionBookTripWithCustomerIdAsNull(){
        final BookTripRequest bookTripRequest = createBookTripRequest(TestDataUtil.createCustomer());
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0031.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionBookTripWithCustomerNotExist(){
        Customer customer= TestDataUtil.createCustomer();
        customer.setId("0");
        final BookTripRequest bookTripRequest = createBookTripRequest(customer);
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0026.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionBookTripWithCustomerHasTripAlready(){
        final BookTripRequest bookTripRequest = createBookTripRequest();
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<BookTripResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, BookTripResponse.class);
        final BookTripResponse bookTripResponse = result.getBody();
        TripPending tripPending = bookTripResponse.getTripPending();
        Assert.assertNotNull(tripPending.getTripId());
        Assert.assertNotNull(tripPending.getCustomer().getBookedTripId());

        final BookTripRequest bookTripRequest2 = createBookTripRequest(tripPending.getCustomer());
        final HttpEntity<BookTripRequest> entity2 = new HttpEntity<>(bookTripRequest2);
        final ResponseEntity<ErrorResponse> result2 = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity2, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0030.getErrorMessage(), result2.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionBookTripWithInvalidStartLocation(){
        final BookTripRequest bookTripRequest = createBookTripRequest(createLocation("0"), createLocation());
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0027.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionBookTripWithInvalidEndLocation(){
        final BookTripRequest bookTripRequest = createBookTripRequest(createLocation(), createLocation("0"));
        final HttpEntity<BookTripRequest> entity = new HttpEntity<>(bookTripRequest);
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(BOOK_TRIP_END_POINT), HttpMethod.POST, entity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0028.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionGetCustomerTripsHistoryWithCustomerNotFound(){
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(GET_CUSTOMER_TRIPS_HISTORY_END_POINT,"0"), HttpMethod.GET, null, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0035.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionGetEmployeeTripsHistoryWithEmployeeNotFound(){
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(GET_EMPLPOYEE_TRIPS_HISTORY_END_POINT,"0"), HttpMethod.GET, null, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0036.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionUpdateTripStatusEmployeeWithNoTripFound(){
        final UpdateTripStatusForEmployeeRequest updateTripStatusForEmployeeRequest = createUpdateTripStatusForEmployeeRequest("0", TripStatus.IN_PROGRESS, "0");
        final HttpEntity<UpdateTripStatusForEmployeeRequest> updateTripStatusForEmployeeRequestEntity = new HttpEntity<>(updateTripStatusForEmployeeRequest);
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(UPDATE_EMPLPOYEE_TRIP_STATUS_END_POINT), HttpMethod.POST, updateTripStatusForEmployeeRequestEntity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0034.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowExceptionUpdateTripStatusEmployeeWithInvalidTripStatus(){
        final UpdateTripStatusForEmployeeRequest updateTripStatusForEmployeeRequest = createUpdateTripStatusForEmployeeRequest("0", TripStatus.PENDING, "0");
        final HttpEntity<UpdateTripStatusForEmployeeRequest> updateTripStatusForEmployeeRequestEntity = new HttpEntity<>(updateTripStatusForEmployeeRequest);
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(UPDATE_EMPLPOYEE_TRIP_STATUS_END_POINT), HttpMethod.POST, updateTripStatusForEmployeeRequestEntity, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0010.getErrorMessage(), result.getBody().getErrorMessage());
    }

    @Test
    public void shouldThrowException(){
        final ResponseEntity<ErrorResponse> result = testRestTemplate.exchange(getApiUrl(GET_ALL_ACTIVE_TRIPS_END_POINT,TripStatus.PENDING.name()), HttpMethod.GET, null, ErrorResponse.class);
        Assert.assertEquals(ErrorCode.E0011.getErrorMessage(), result.getBody().getErrorMessage());

    }
}
