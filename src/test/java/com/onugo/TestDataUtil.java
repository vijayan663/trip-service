package com.onugo;

import com.onugo.model.document.Admin;
import com.onugo.model.document.Contact;
import com.onugo.model.document.Customer;
import com.onugo.model.document.Employee;
import com.onugo.model.document.Route;
import com.onugo.model.document.Vehicle;
import com.onugo.model.document.VehicleType;
import org.apache.commons.lang3.RandomStringUtils;

public class TestDataUtil {
    private static String createRandomNumericString(String prefix, int maxLength) {
        String output = RandomStringUtils.randomNumeric(maxLength, maxLength);
        if (prefix != null) {
            output = prefix + output;
        }
        return output;
    }

    private static String createRandomAlphabeticString(String prefix, int maxLength) {
        String output = RandomStringUtils.randomAlphabetic(maxLength, maxLength);
        if (prefix != null) {
            output = prefix + output;
        }
        return output;
    }

    private static String getRandomNumber(){
        return createRandomNumericString("",10);
    }
    private static String getRandomNameWithPrefix(String prefix){
        return createRandomAlphabeticString(prefix,10);
    }

    private static String getRandomNameWithSuffix(String suffix){
        return createRandomAlphabeticString("",10) + suffix;
    }

    public static Admin createAdmin(){
        Admin admin = new Admin();
        admin.setFirstName(getRandomNameWithPrefix("firstName_"));
        admin.setLastName(getRandomNameWithPrefix("lastName_"));
        admin.setContact(createContact());
        return admin;
    }

    public static Admin createAdmin(String email){
        Admin admin = new Admin();
        admin.setFirstName(getRandomNameWithPrefix("firstName_"));
        admin.setLastName(getRandomNameWithPrefix("lastName_"));
        admin.setContact(createContact(email));
        return admin;
    }

    public static Contact createContact(){
        Contact contact = new Contact();
        contact.setEmail(getRandomNameWithSuffix("@gmail.com"));
        contact.setPhoneNumber(getRandomNumber());
        return contact;
    }

    public static Contact createContact(String email){
        Contact contact = new Contact();
        contact.setEmail(email);
        contact.setPhoneNumber(getRandomNumber());
        return contact;
    }

    public static Admin updateAdmin(Admin admin, boolean isUpdateEmail){
        admin.setFirstName(getRandomNameWithPrefix("firstName_"));
        admin.setLastName(getRandomNameWithPrefix("lastName_"));
        admin.setContact(updateContact(admin.getContact(), isUpdateEmail));
        return admin;
    }

    public static Contact updateContact(Contact contact, boolean isUpdateEmail){
        if(isUpdateEmail) {
            contact.setEmail(getRandomNameWithSuffix("@gmail.com"));
        }
        contact.setPhoneNumber(getRandomNumber());
        return contact;
    }

    public static Employee createEmployee(){
        Employee employee = new Employee();
        employee.setFirstName(getRandomNameWithPrefix("firstName_"));
        employee.setLastName(getRandomNameWithPrefix("lastName_"));
        employee.setContact(createContact());
        employee.setVehicle(createVehicle());
        return employee;
    }

    public static Vehicle createVehicle(){
        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleId(getRandomNameWithPrefix("vehicleId_"));
        vehicle.setVehicleType(VehicleType.HAT);
        return vehicle;
    }

    public static Employee createEmployee(String email){
        Employee employee = new Employee();
        employee.setFirstName(getRandomNameWithPrefix("firstName_"));
        employee.setLastName(getRandomNameWithPrefix("lastName_"));
        employee.setContact(createContact(email));
        return employee;
    }

    public static Employee updateEmployee(Employee employee, boolean isUpdateEmail){
        employee.setFirstName(getRandomNameWithPrefix("firstName_"));
        employee.setLastName(getRandomNameWithPrefix("lastName_"));
        employee.setContact(updateContact(employee.getContact(), isUpdateEmail));
        return employee;
    }

    public static Customer createCustomer(){
        Customer customer = new Customer();
        customer.setFirstName(getRandomNameWithPrefix("firstName_"));
        customer.setLastName(getRandomNameWithPrefix("lastName_"));
        customer.setContact(createContact());
        return customer;
    }

    public static Customer createCustomer(String email){
        Customer customer = new Customer();
        customer.setFirstName(getRandomNameWithPrefix("firstName_"));
        customer.setLastName(getRandomNameWithPrefix("lastName_"));
        customer.setContact(createContact(email));
        return customer;
    }

    public static Customer updateCustomer(Customer customer, boolean isUpdateEmail){
        customer.setFirstName(getRandomNameWithPrefix("firstName_"));
        customer.setLastName(getRandomNameWithPrefix("lastName_"));
        customer.setContact(updateContact(customer.getContact(), isUpdateEmail));
        return customer;
    }

    public static String getRandomRouteAddress(){
        return createRandomAlphabeticString("RouteAddress_",10);
    }

    public static Route createRoute(String address){
        final Route route = new Route();
        route.setRouteAddress(address);
        return route;
    }

    public static Route createRoute(){
        final Route route = new Route();
        route.setRouteAddress(getRandomRouteAddress());
        return route;
    }
}
