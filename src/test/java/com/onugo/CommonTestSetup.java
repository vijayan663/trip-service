package com.onugo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes=com.onugo.TripServiceApplication.class)
public class CommonTestSetup {
    @Autowired
    protected TestRestTemplate testRestTemplate;

    @Test
    public void TestMethod(){
        Assert.assertTrue(true);
    }
}
