package com.onugo.service;

import com.onugo.constants.ErrorCode;
import com.onugo.exception.OnugoException;
import com.onugo.model.document.Admin;
import com.onugo.model.document.Customer;
import com.onugo.model.document.Employee;
import com.onugo.model.request.AdminRequest;
import com.onugo.model.request.CustomerRequest;
import com.onugo.model.request.EmployeRequest;
import com.onugo.repository.AdminRepository;
import com.onugo.repository.CustomerRepository;
import com.onugo.repository.EmployeeRepository;
import com.onugo.util.OnUGoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final CustomerRepository customerRepository;
    private final EmployeeRepository employeeRepository;
    private final AdminRepository adminRepository;

    public Customer createCustomer(final CustomerRequest customerRequest){
        Customer customer = customerRequest.getCustomer();
        customer.setId(OnUGoUtil.generateRandomUUID());
        customer.setTripIds(new ArrayList<>());
        validateCustomerEmail(customer.getContact().getEmail());
        customerRepository.insert(customer);
        return customer;
    }

    private void validateCustomerEmail(String email){
        Customer customer = customerRepository.findByCustomerEmail(email).orElse(null);
        if(customer!=null){
            throw new OnugoException(ErrorCode.E0018);
        }
    }

    private void validateCustomerEmail(String email, String existingCustomerId){
        Customer customer = customerRepository.findByCustomerEmail(email).orElse(null);
        if(customer!=null && !customer.getId().equals(existingCustomerId)){
            throw new OnugoException(ErrorCode.E0018);
        }
    }

    public boolean updateCustomer(final CustomerRequest customerRequest){
        Customer customer = customerRequest.getCustomer();
        if(StringUtils.isEmpty(customer.getId())){
            throw new OnugoException(ErrorCode.E0019);
        }
        validateCustomerId(customer.getId());
        validateCustomerEmail(customer.getContact().getEmail(), customer.getId());
        customerRepository.save(customer);
        return true;
    }

    private void validateCustomerId(String id){
        Customer existingCustomer = getCustomer(id);
        if(existingCustomer==null){
            throw new OnugoException(ErrorCode.E0020);
        }
    }

    public Customer getCustomer(String customerId){
        return customerRepository.findById(customerId).orElse(null);
    }

    public Employee createEmployee(EmployeRequest employeRequest){
        final Employee employee = employeRequest.getEmployee();
        employee.setId(OnUGoUtil.generateRandomUUID());
        employee.setTripIds(new ArrayList<>());
        validateEmployeeEmail(employee.getContact().getEmail());
        employeeRepository.insert(employee);
        return employee;
    }

    private void validateEmployeeEmail(String email){
        Employee employee = employeeRepository.findByEmployeeEmail(email).orElse(null);
        if(employee!=null){
            throw new OnugoException(ErrorCode.E0018);
        }
    }

    private void validateEmployeeEmail(String email, String existingEmployeeId){
        Employee employee = employeeRepository.findByEmployeeEmail(email).orElse(null);
        if(employee!=null && !employee.getId().equals(existingEmployeeId)){
            throw new OnugoException(ErrorCode.E0018);
        }
    }

    public boolean updateEmployee(EmployeRequest employeRequest){
        final Employee employee = employeRequest.getEmployee();
        if(StringUtils.isEmpty(employee.getId())){
            throw new OnugoException(ErrorCode.E0021);
        }
        validateEmployeeId(employee.getId());
        validateEmployeeEmail(employee.getContact().getEmail(), employee.getId());
        employeeRepository.save(employee);
        return true;
    }

    private void validateEmployeeId(String id){
        Employee existingEmployee = getEmployee(id);
        if(existingEmployee==null){
            throw new OnugoException(ErrorCode.E0022);
        }
    }
    public Employee getEmployee(String employeeId){
        return employeeRepository.findById(employeeId).orElse(null);
    }

    public List<Employee> getAllEmployee(){
        return employeeRepository.findAll();
    }

    public Admin createAdmin(AdminRequest adminRequest){
        final Admin admin = adminRequest.getAdmin();
        admin.setId(OnUGoUtil.generateRandomUUID());
        validateAdminEmail(admin.getContact().getEmail());
        adminRepository.insert(admin);
        return admin;
    }

    private void validateAdminEmail(String email){
        Admin admin = adminRepository.findByAdminEmail(email).orElse(null);
        if(admin!=null){
            throw new OnugoException(ErrorCode.E0018);
        }
    }

    private void validateAdminEmail(String email, String existingAdminId){
        Admin admin = adminRepository.findByAdminEmail(email).orElse(null);
        if(admin!=null && !admin.getId().equals(existingAdminId)){
            throw new OnugoException(ErrorCode.E0018);
        }
    }

    public boolean updateAdmin(AdminRequest adminRequest){
        final Admin admin = adminRequest.getAdmin();
        if(StringUtils.isEmpty(admin.getId())){
            throw new OnugoException(ErrorCode.E0023);
        }
        validateAdminId(admin.getId());
        validateAdminEmail(admin.getContact().getEmail(), admin.getId());
        adminRepository.save(admin);
        return true;
    }

    private void validateAdminId(String id){
        Admin existingAdmin = getAdmin(id);
        if(existingAdmin==null){
            throw new OnugoException(ErrorCode.E0024);
        }
    }

    public Admin getAdmin(String adminId){
        return adminRepository.findById(adminId).orElse(null);
    }

    public boolean deleteAdmin(String adminId){
        adminRepository.deleteById(adminId);
        return true;
    }

    public boolean deleteCustomer(String customerId){
        customerRepository.deleteById(customerId);
        return true;
    }

    public boolean deleteEmployee(String employeeId){
        employeeRepository.deleteById(employeeId);
        return true;
    }

    public List<Admin> getAllAdmins(){return adminRepository.findAll();}

    public List<Customer> getAllCustomers(){return customerRepository.findAll();}

}
