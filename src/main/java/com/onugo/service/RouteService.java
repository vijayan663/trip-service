package com.onugo.service;

import com.onugo.constants.ErrorCode;
import com.onugo.exception.OnugoException;
import com.onugo.model.document.Route;
import com.onugo.model.request.RouteRequest;
import com.onugo.repository.RouteRepository;
import com.onugo.util.OnUGoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RouteService {
    private final RouteRepository routeRepository;

    public Route createRoute(RouteRequest routeRequest){
        final Route route = routeRequest.getRoute();
        route.setRouteAddress(route.getRouteAddress().toUpperCase());
        validateExistingRouteByAddress(route.getRouteAddress(), ErrorCode.E0013);
        route.setId(OnUGoUtil.generateRandomUUID());
        routeRepository.insert(route);
        return route;
    }

    public List<Route> createRoutes(List<Route> routes){
        List<String> addresses = new ArrayList<>();
        routes.forEach(route->{
            route.setRouteAddress(route.getRouteAddress().toUpperCase());
            route.setId(OnUGoUtil.generateRandomUUID());
            addresses.add(route.getRouteAddress());
        });

        validateExistingRoutesByAddress(addresses, ErrorCode.E0013);
        routeRepository.insert(routes);
        return routes;
    }

    private void validateExistingRoutesByAddress(List<String> routeAddresses, ErrorCode errorCode){
        final List<Route> existingRoutes = routeRepository.findAllByRouteAddressIn(routeAddresses).orElse(null);
        if(!CollectionUtils.isEmpty(existingRoutes)){
            throw new OnugoException(errorCode);
        }
    }


    private void validateExistingRouteByAddress(String address, ErrorCode errorCode){
        final Route existingRoute = routeRepository.findByRouteAddressEquals(address).orElse(null);
        if( existingRoute!=null){
            throw new OnugoException(errorCode);
        }
    }

    private void validateExistingRouteByAddress(String address, String routeId, ErrorCode errorCode){
        final Route existingRoute = routeRepository.findByRouteAddressEquals(address).orElse(null);
        if( existingRoute!=null && !existingRoute.getId().equals(routeId)){
            throw new OnugoException(errorCode);
        }
    }

    private void validateExistingRouteById(String routeId, ErrorCode errorCode){
        if(getRouteById(routeId)==null){
            throw new OnugoException(errorCode);
        }
    }

    private Route getRouteById(String routeId){
        return routeRepository.findById(routeId).orElse(null);
    }


    public boolean updateRoute(RouteRequest routeRequest){
        final Route route = routeRequest.getRoute();
        if(StringUtils.isEmpty(route.getId())){
            throw new OnugoException(ErrorCode.E0017);
        }
        route.setRouteAddress(route.getRouteAddress().toUpperCase());
        validateExistingRouteById(route.getId(), ErrorCode.E0014);
        validateExistingRouteByAddress(route.getRouteAddress(), route.getId(), ErrorCode.E0015);
        routeRepository.save(route);
        return true;
    }

    public boolean deleteRoute(String routeId){
        validateExistingRouteById(routeId, ErrorCode.E0016);
        routeRepository.deleteById(routeId);
        return true;
    }

    public Route getRoute(String routeId){
        return routeRepository.findById(routeId).orElse(null);
    }

    public Route getRouteByAddress(String routeAddress){
        return routeRepository.findByRouteAddressEquals(routeAddress.toUpperCase()).orElse(null);
    }

    public List<Route> getAllRoutes(){
        return routeRepository.findAll();
    }
}
