package com.onugo.service;

import com.onugo.constants.ErrorCode;
import com.onugo.exception.OnugoException;
import com.onugo.mapper.TripMapper;
import com.onugo.model.document.Route;
import com.onugo.model.request.AcceptTripRequest;
import com.onugo.model.request.BookTripRequest;
import com.onugo.model.request.UpdateTripStatusForEmployeeRequest;
import com.onugo.model.response.BookTripResponse;
import com.onugo.model.document.Customer;
import com.onugo.model.response.CustomerBookedTripResponse;
import com.onugo.model.document.Employee;
import com.onugo.model.document.Location;
import com.onugo.model.document.TripActive;
import com.onugo.model.document.TripHistory;
import com.onugo.model.document.TripPending;
import com.onugo.model.document.TripStatus;
import com.onugo.repository.CustomerRepository;
import com.onugo.repository.EmployeeRepository;
import com.onugo.repository.TripActiveRepository;
import com.onugo.repository.TripHistoryRepository;
import com.onugo.repository.TripPendingRepository;
import com.onugo.util.OnUGoUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class TripService {
    private final TripPendingRepository tripPendingRepository;
    private final TripActiveRepository tripActiveRepository;
    private final TripHistoryRepository tripHistoryRepository;
    private final EmployeeRepository employeeRepository;
    private final CustomerRepository customerRepository;
    private final UserService userService;
    private final RouteService routeService;
    private final TripMapper tripMapper;
    private static final int START_TIME_LIMIT_IN_MINUTES = 30;
    private static final int END_TIME_LIMIT_IN_MINUTES = 240;

    public BookTripResponse bookTrip(BookTripRequest bookTripRequest){
        final Customer customer = bookTripRequest.getCustomer();
        validateStartTime(bookTripRequest.getStartTime());
        validateCustomer(customer);
        validateRoute(bookTripRequest.getStartLocation().getAddress(), ErrorCode.E0027);
        validateRoute(bookTripRequest.getEndLocation().getAddress(), ErrorCode.E0028);


        final TripPending tripPending = buildTripPending(customer, TripStatus.PENDING, bookTripRequest.getStartLocation(), bookTripRequest.getEndLocation(), bookTripRequest.getStartTime());
        tripPendingRepository.save(tripPending);

        customer.setBookedTripId(tripPending.getTripId());
        customerRepository.save(customer);
        return buildBookTripResponse(tripPending);
    }

    private void validateStartTime(Date date){
        Date currentDate = new Date();
        Date maximumDate = DateUtils.addMinutes(currentDate, END_TIME_LIMIT_IN_MINUTES);
        if(date.compareTo(maximumDate)>0){
            throw new OnugoException(ErrorCode.E0029);
        }
    }

    private void validateCustomer(Customer customer){
        if(StringUtils.isEmpty(customer.getId())){
            throw new OnugoException(ErrorCode.E0031);
        }
        Customer customerResponse = userService.getCustomer(customer.getId());
        if(customerResponse==null){
            throw new OnugoException(ErrorCode.E0026);
        }else if(!StringUtils.isEmpty(customerResponse.getBookedTripId())){
            throw new OnugoException(ErrorCode.E0030);
        }
    }
    private void validateRoute(String address, ErrorCode errorCode){
        Route route = routeService.getRouteByAddress(address);
        if(route==null){
            throw new OnugoException(errorCode);
        }
    }
    public boolean cancelTrip(final String tripId){
        final TripHistory tripHistory;
        final Employee employee;
        final Customer customer;
        CustomerBookedTripResponse customerBookedTripResponse = getCustomerBookedTrip(tripId);
        if(customerBookedTripResponse.getTripPending()!=null){
            employee = customerBookedTripResponse.getTripPending().getEmployee();
            customer = customerBookedTripResponse.getTripPending().getCustomer();
            tripHistory = tripMapper.tripPendingToTripHistory(customerBookedTripResponse.getTripPending());
            setTripHistoryDetailsForCancel(tripHistory, customer);
            tripHistoryRepository.insert(tripHistory);
            tripPendingRepository.deleteById(tripId);

        }else {
            employee = customerBookedTripResponse.getTripActive().getEmployee();
            customer = customerBookedTripResponse.getTripActive().getCustomer();
            tripHistory = tripMapper.tripActiveToTripHistory(customerBookedTripResponse.getTripActive());
            setTripHistoryDetailsForCancel(tripHistory, customer);
            tripHistoryRepository.insert(tripHistory);
            tripActiveRepository.deleteById(tripId);
        }
        if(employee!=null && !StringUtils.isEmpty(employee.getOngoingTripId())){
            employee.setOngoingTripId(null);
            employeeRepository.save(employee);
        }
        if(!StringUtils.isEmpty(customer.getBookedTripId())){
            customer.setBookedTripId(null);
            customer.getTripIds().add(tripId);
            customerRepository.save(customer);
        }
        return true;
    }

    private void setTripHistoryDetailsForCancel(TripHistory tripHistory, Customer customer){
        tripHistory.setTripStatus(TripStatus.CANCELLED);
        tripHistory.setCancelledBy(customer.getId());
        tripHistory.setCreatedTime(new Date());
    }

    public boolean acceptTrip(AcceptTripRequest acceptTripRequest){
        final TripActive tripActive;
        final String tripId = acceptTripRequest.getTripId();
        final Employee employee = acceptTripRequest.getEmployee();
        final TripPending tripPending = getTripPending(tripId);
        if(tripPending ==null){
            throw new OnugoException(ErrorCode.E0033);
        }
        final List<String> tripIds = employee.getTripIds();
        final String currentTripId = tripPending.getTripId();

        tripIds.add(currentTripId);
        employee.setOngoingTripId(currentTripId);
        employeeRepository.save(employee);

        tripActive = tripMapper.tripPendingToTripActive(tripPending);
        tripActive.setTripStatus(TripStatus.SCHEDULED);
        tripActive.setAcceptedBy(employee.getId());
        tripActive.setCreatedTime(new Date());
        tripActive.setEmployee(employee);
        tripActiveRepository.insert(tripActive);

        tripPendingRepository.deleteById(tripPending.getTripId());

        return true;
    }

    public CustomerBookedTripResponse getCustomerBookedTrip(String tripId){
        final TripActive tripActive;
        final TripPending tripPending = getTripPending(tripId);
        if(tripPending == null){
            tripActive = getTripActive(tripId);
            if(tripActive == null){
                throw new OnugoException(ErrorCode.E0007);
            }
            return buildCustomerBookedTripResponse(null, tripActive);
        }else{
            return buildCustomerBookedTripResponse(tripPending, null);
        }
    }

    public List<TripPending> getAllPendingTrips(){
        return tripPendingRepository.findAll();
    }

    public List<TripHistory> getCustomerTripsHistory(String customerId){
        final String tripIds;
        final Customer customer = customerRepository.findById(customerId).orElse(null);
        if(customer == null){
            throw new OnugoException(ErrorCode.E0035);
        }
        if(CollectionUtils.isEmpty(customer.getTripIds())){
            throw new OnugoException(ErrorCode.E0037);
        }
        tripIds = OnUGoUtil.getCommaSeparatedStringFromList(customer.getTripIds());
        return tripHistoryRepository.findAllByTripIdIn(tripIds);
    }

    public List<TripHistory> getEmployeeTripsHistory(String employeeId){
        final Employee employee = employeeRepository.findById(employeeId).orElse(null);
        final String tripIds;
        if(employee == null){
            throw new OnugoException(ErrorCode.E0036);
        }
        tripIds = OnUGoUtil.getCommaSeparatedStringFromList(employee.getTripIds());
        return tripHistoryRepository.findAllByTripIdIn(tripIds);
    }

    public boolean updateEmployeeTripStatus(UpdateTripStatusForEmployeeRequest updateTripStatusForEmployeeRequest){
        final TripStatus tripStatus = updateTripStatusForEmployeeRequest.getTripStatus();
        final String tripId = updateTripStatusForEmployeeRequest.getTripId();

        validTripStatusForEmployeeUpdate(tripStatus);

        final TripActive tripActive = getTripActive(tripId);
        if(tripActive == null){
            throw new OnugoException(ErrorCode.E0034);
        }
        if(TripStatus.IN_PROGRESS.equals(tripStatus)){
            tripActive.setTripStatus(TripStatus.IN_PROGRESS);
            tripActive.setUpdateTime(new Date());
            tripActiveRepository.save(tripActive);
        }else{
            final TripHistory tripHistory = tripMapper.tripActiveToTripHistory(tripActive);
            tripHistory.setTripStatus(TripStatus.COMPLETED);
            tripHistory.setCompletedBy(tripHistory.getEmployee().getId());
            tripHistoryRepository.insert(tripMapper.tripActiveToTripHistory(tripActive));
            tripActiveRepository.deleteById(tripId);
        }
        return true;
    }

    public List<TripActive> getAllActiveTrips(TripStatus tripStatus){
        validateActiveTripStatus(tripStatus);
        if(TripStatus.SCHEDULED.equals(tripStatus)) {
            return tripActiveRepository.findAllByTripStatusEquals(TripStatus.SCHEDULED).orElse(null);
        }else{
            return tripActiveRepository.findAllByTripStatusEquals(TripStatus.IN_PROGRESS).orElse(null);
        }
    }

    private void validateActiveTripStatus(TripStatus tripStatus){
        if(!TripStatus.SCHEDULED.equals(tripStatus) && !TripStatus.IN_PROGRESS.equals(tripStatus)){
            throw new OnugoException(ErrorCode.E0011);
        }
    }

    private CustomerBookedTripResponse buildCustomerBookedTripResponse(TripPending tripPending, TripActive tripActive){
        CustomerBookedTripResponse customerBookedTripResponse = new CustomerBookedTripResponse();
        customerBookedTripResponse.setMessage("Successfully retrieved booked trip details");
        customerBookedTripResponse.setTripPending(tripPending);
        customerBookedTripResponse.setTripActive(tripActive);
        return customerBookedTripResponse;
    }

    private BookTripResponse buildBookTripResponse(TripPending tripPending){
        final BookTripResponse bookTripResponse = new BookTripResponse();
        bookTripResponse.setTripPending(tripPending);
        bookTripResponse.setMessage("Booking successfully initiated");
        return bookTripResponse;
    }

    private TripPending buildTripPending(Customer customer, TripStatus tripStatus, Location startLocation, Location endLocation, Date startTime) {
        final TripPending tripPending = new TripPending();
        tripPending.setTripId(OnUGoUtil.generateRandomUUID());
        tripPending.setCustomer(customer);
        tripPending.getCustomer().setBookedTripId(tripPending.getTripId());
        tripPending.setTripStatus(tripStatus);
        tripPending.setStartLocation(startLocation);
        tripPending.setEndLocation(endLocation);
        tripPending.setStartTime(startTime);
        tripPending.setCreatedBy(customer.getId());
        tripPending.setCreatedTime(new Date());
        return tripPending;
    }

    private void validTripStatusForEmployeeUpdate(TripStatus tripStatus){
        if(!TripStatus.IN_PROGRESS.equals(tripStatus) && !TripStatus.COMPLETED.equals(tripStatus)){
            throw new OnugoException(ErrorCode.E0010);
        }
    }

    public TripHistory getTripHistory(final String tripId){
        return tripHistoryRepository.findById(tripId).orElse(null);
    }

    public TripActive getTripActive(final String tripId){
        return tripActiveRepository.findById(tripId).orElse(null);
    }

    public TripPending getTripPending(final String tripId){
        return tripPendingRepository.findById(tripId).orElse(null);
    }
}