package com.onugo.constants;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum ErrorCode {
    E0001("E0001", "No Employees available at the moment. Please try again later!"),
    E0002("E0002", "Trip was already started and it cannot be cancelled!"),
    E0003("E0003", "Trip was already cancelled and it cannot be cancelled again!"),
    E0004("E0004", "Trip was already completed and it cannot be cancelled!"),
    E0005("E0005", "Trip is not in proper status to schedule!"),
    E0006("E0006", "Customer Record not found to find booked trip details"),
    E0007("E0007", "No trip exists for this customer"),
    E0008("E0008", "Employee Record not found to find trip details"),
    E0009("E0009", "No trip exists for this employee"),
    E0010("E0010", "Invalid Trip status to update for an Employee"),
    E0011("E0011", "Invalid Trip status to fetch from Active Trips"),
    E0012("E0012", "Invalid Trip status to fetch from Trip History"),
    E0013("E0013", "Route already exists"),
    E0014("E0014", "Route does not exist to update"),
    E0015("E0015", "Route already exists, Please update with different address"),
    E0016("E0016", "Route does not exist to delete"),
    E0017("E0017", "Route Id should not be null for update"),
    E0018("E0018", "Email Already exists, please provide different email address"),
    E0019("E0019", "Customer Id should not be null for update"),
    E0020("E0020", "Customer does not exist to update"),
    E0021("E0021", "Employee Id should not be null for update"),
    E0022("E0022", "Employee does not exist to update"),
    E0023("E0023", "Admin Id should not be null for update"),
    E0024("E0024", "Admin does not exist to update"),
    E0025("E0025", "Start Time should be greater than 30 minutes from current time for booking"),
    E0026("E0026", "Customer record does not exist for booking"),
    E0027("E0027", "Start Location address not found in system"),
    E0028("E0028", "End Location address not found in system"),
    E0029("E0029", "Start Time should be lesser than or equal to 4 hours from current time for booking"),
    E0030("E0030", "New Trip booking is not possible, since already trip is pending for the customer which is not yet completed"),
    E0031("E0031", "Customer Id should not be null for booking trip"),
    E0032("E0032", "No Pending or Active Trips available"),
    E0033("E0033", "Pending Trip not available to Accept"),
    E0034("E0034", "Active Trip does not exist to update for employee"),
    E0035("E0035", "Customer Record not found to find trip history details"),
    E0036("E0036", "Employee Record not found to find trip history details"),
    E0037("E0037", "No Trip History exist!");
    private final String errorCode;
    private final String errorMessage;
    ErrorCode(String errorCode, String errorMessage){
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}