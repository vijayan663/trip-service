package com.onugo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * The class to implement swagger ui for the endpoints.
 */
@Configuration
@Slf4j
public class SwaggerConfig {

    private static final String API_INFORMATION_FILE = "api-information.md";


    /**
     * Api.
     *
     * @return the docket
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.onugo.controller"))
                .paths(PathSelectors.regex("/.*")).build().apiInfo(apiEndPointsInfo())
                .useDefaultResponseMessages(false);
    }

    /**
     * Api end points info.
     *
     * @return the api info
     */
    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("" +
                "Manage Device Registartion Service").description(getApiDescription())
                .version("1.0.0").build();
    }

    /**
     * Reads the description from MD file present in the resources folder
     *
     * @return ""
     */
    private String getApiDescription() {
        try {
            try (InputStream informationInputStream = new ClassPathResource(API_INFORMATION_FILE).getInputStream()) {
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(informationInputStream))) {
                    return bufferedReader.lines().collect(Collectors.joining("\n"));
                }
            }
        } catch (IOException e) {
            log.warn("Could not read api information from file");
            return "";
        }
    }

    /**
     * Ui config.
     *
     * @return the ui configuration
     */
    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder().displayRequestDuration(true).validatorUrl("").build();
    }
}