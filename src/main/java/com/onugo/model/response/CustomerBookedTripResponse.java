package com.onugo.model.response;

import com.onugo.model.document.TripActive;
import com.onugo.model.document.TripPending;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerBookedTripResponse {
    private String message;
    private TripPending tripPending;
    private TripActive tripActive;
}
