package com.onugo.model.response;

import com.onugo.model.document.Route;

import java.util.List;

public class GetAllRouteResponse {
    private String message;
    private boolean status;
    private List<Route> routes;
}
