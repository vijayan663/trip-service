package com.onugo.model.response;

import com.onugo.model.document.TripPending;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookTripResponse {
    private String message;
    private TripPending tripPending;
}
