package com.onugo.model.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CancelTripRequest {
    @NotNull
    private String tripId;
}
