package com.onugo.model.request;

import com.onugo.model.document.Employee;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AcceptTripRequest {
    @NotNull
    private String tripId;
    @NotNull
    private String acceptedBy;
    @NotNull
    private Employee employee;
}
