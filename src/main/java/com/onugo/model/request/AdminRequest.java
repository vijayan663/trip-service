package com.onugo.model.request;

import com.onugo.model.document.Admin;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;

@Getter
@Setter
public class AdminRequest {
    @Valid
    private Admin admin;
}
