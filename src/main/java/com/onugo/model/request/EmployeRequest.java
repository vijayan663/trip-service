package com.onugo.model.request;

import com.onugo.model.document.Employee;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;

@Getter
@Setter
public class EmployeRequest {
    @Valid
    private Employee employee;
}
