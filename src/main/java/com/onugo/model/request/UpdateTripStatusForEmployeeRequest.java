package com.onugo.model.request;

import com.onugo.model.document.TripStatus;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UpdateTripStatusForEmployeeRequest {
    @NotNull
    private String tripId;
    @NotNull
    private TripStatus tripStatus;
    @NotNull
    private String updateBy;
}
