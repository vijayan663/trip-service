package com.onugo.model.request;

import com.onugo.model.document.Customer;
import com.onugo.model.document.Location;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class BookTripRequest {
    @NotNull
    private Customer customer;
    @NotNull
    private Location startLocation;
    @NotNull
    private Location endLocation;
    @NotNull
    private Date startTime;
}
