package com.onugo.model.request;

import com.onugo.model.document.Route;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;

@Getter
@Setter
public class RouteRequest {
    @Valid
    private Route route;
}
