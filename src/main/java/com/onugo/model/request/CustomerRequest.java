package com.onugo.model.request;

import com.onugo.model.document.Customer;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;

@Getter
@Setter
public class CustomerRequest {
    @Valid
    private Customer customer;
}
