package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class Vehicle {
    @NotNull
    private String vehicleId;
    @NotNull
    private VehicleType vehicleType;
}
