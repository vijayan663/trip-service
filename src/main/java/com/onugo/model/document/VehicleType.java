package com.onugo.model.document;

public enum VehicleType {
    HAT("HATCHBACK"),
    SED("SEDAN"),
    SUV("SUV"),
    MPV("MPV");



    private String value;

    VehicleType(String value){
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }


}
