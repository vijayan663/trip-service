package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class Trip {
    @Id
    private String tripId;
    @NotNull
    private TripStatus tripStatus;
    @NotNull
    private Customer customer;
    @NotNull
    private Employee employee;
    @NotNull
    private Location startLocation;
    @NotNull
    private Location endLocation;
    @NotNull
    private Date startTime;
    private Date endTime;
    private Date createdTime;
}
