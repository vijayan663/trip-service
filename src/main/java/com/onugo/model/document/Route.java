package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Document(collection = "route")
@Getter
@Setter
public class Route {
    @Id
    private String id;
    @NotNull
    private String routeAddress;
}
