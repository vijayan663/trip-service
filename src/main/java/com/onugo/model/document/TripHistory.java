package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "trip_history")
@Getter
@Setter
public class TripHistory extends Trip{
    private String cancelledBy;
    private String completedBy;
}
