package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Document(collection = "trip_active")
@Getter
@Setter
public class TripActive extends Trip{
    private Date updateTime;
    @NotNull
    private String acceptedBy;
}