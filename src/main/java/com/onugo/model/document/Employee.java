package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;

@Document(collection = "employee")
@Getter
@Setter
public class Employee {
    @Id
    private String id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private List<String> tripIds;
    private String ongoingTripId;
    private Vehicle vehicle;
    @NotNull
    private Contact contact;
}
