package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "trip_pending")
@Getter
@Setter
public class TripPending extends Trip{
    private String createdBy;
}

