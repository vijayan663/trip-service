package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class Contact {
    @NotNull
    private String phoneNumber;
    @NotNull
    private String email;
}
