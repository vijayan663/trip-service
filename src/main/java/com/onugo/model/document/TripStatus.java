package com.onugo.model.document;

public enum TripStatus {
     PENDING,
     SCHEDULED,
     CANCELLED,
     IN_PROGRESS,
     COMPLETED
}