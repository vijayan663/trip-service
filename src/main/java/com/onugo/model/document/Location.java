package com.onugo.model.document;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class Location {
    private String id;

    @NotNull
    private String address;
}
