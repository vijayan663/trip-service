package com.onugo.repository;

import com.onugo.model.document.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {
    Optional<List<Employee>> findAllByOngoingTripIdIsNull();
    @Query("{'contact.email' : {$eq : ?0}}")
    Optional<Employee> findByEmployeeEmail(String email);
}
