package com.onugo.repository;

import com.onugo.model.document.TripPending;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface TripPendingRepository extends MongoRepository<TripPending, String> {
    @Query("'customer.id' : ?")
    Optional<List<TripPending>> findAllBookingTripsForCustomer(String customerId);
}
