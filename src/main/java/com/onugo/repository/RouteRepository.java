package com.onugo.repository;

import com.onugo.model.document.Route;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RouteRepository extends MongoRepository<Route, String> {
    Optional<Route> findByRouteAddressEquals(String routeAddress);
    Optional<List<Route>> findAllByRouteAddressIn(List<String> routeAddresses);
}
