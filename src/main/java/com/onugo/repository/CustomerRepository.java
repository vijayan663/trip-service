package com.onugo.repository;

import com.onugo.model.document.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {
    @Query("{'contact.email' : {$eq : ?0}}")
    Optional<Customer> findByCustomerEmail(String email);
}
