package com.onugo.repository;

import com.onugo.model.document.TripActive;
import com.onugo.model.document.TripStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TripActiveRepository extends MongoRepository<TripActive, String> {
    Optional<List<TripActive>> findAllByTripStatusEquals(TripStatus tripStatus);
}
