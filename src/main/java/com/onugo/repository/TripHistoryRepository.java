package com.onugo.repository;


import com.onugo.model.document.TripHistory;
import com.onugo.model.document.TripStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TripHistoryRepository extends MongoRepository<TripHistory, String> {
    Optional<List<TripHistory>> findAllByTripStatusEquals(TripStatus tripStatus);
    List<TripHistory> findAllByTripIdIn(String tripIds);
}
