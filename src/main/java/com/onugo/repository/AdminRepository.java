package com.onugo.repository;

import com.onugo.model.document.Admin;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminRepository extends MongoRepository<Admin, String> {
    @Query("{'contact.email' : {$eq : ?0}}")
    Optional<Admin> findByAdminEmail(String email);
}
