package com.onugo.util;


import java.util.List;
import java.util.UUID;

public class OnUGoUtil {
    public static String generateRandomUUID(){
        return UUID.randomUUID().toString();
    }

    public static String getCommaSeparatedStringFromList(List<String> tripIds){
        return String.join(",", tripIds);
    }


}
