package com.onugo.mapper;

import com.onugo.model.document.TripActive;
import com.onugo.model.document.TripHistory;
import com.onugo.model.document.TripPending;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface TripMapper {
    TripHistory tripPendingToTripHistory(TripPending tripPending);
    TripHistory tripActiveToTripHistory(TripActive tripActive);
    TripActive tripPendingToTripActive(TripPending tripPending);
}
