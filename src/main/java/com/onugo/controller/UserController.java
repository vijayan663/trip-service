package com.onugo.controller;

import com.onugo.model.document.Admin;
import com.onugo.model.document.Customer;
import com.onugo.model.document.Employee;
import com.onugo.model.request.AdminRequest;
import com.onugo.model.request.CustomerRequest;
import com.onugo.model.request.EmployeRequest;
import com.onugo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@CrossOrigin
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping(value = "/admin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Admin> createAdmin(@RequestBody @Valid AdminRequest adminRequest){
        return ResponseEntity.ok(userService.createAdmin(adminRequest));
    }

    @PutMapping(value = "/admin", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> updateAdmin(@RequestBody @Valid AdminRequest adminRequest){
        return ResponseEntity.ok(userService.updateAdmin(adminRequest));
    }

    @GetMapping(value = "/admin/{adminId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Admin> getAdmin(@PathVariable @NotNull String adminId){
        return ResponseEntity.ok(userService.getAdmin(adminId));
    }

    @DeleteMapping(value = "/admin/{adminId}")
    public ResponseEntity<Boolean> deleteAdmin(@PathVariable @NotNull String adminId){
        return ResponseEntity.ok(userService.deleteAdmin(adminId));
    }

    @GetMapping(value = "/admins", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Admin>> getAllAdmins(){
        return ResponseEntity.ok(userService.getAllAdmins());
    }

    @PostMapping(value = "/employee", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> createEmployee(@RequestBody @Valid EmployeRequest employeRequest){
        return ResponseEntity.ok(userService.createEmployee(employeRequest));
    }

    @PutMapping(value = "/employee", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> updateEmployee(@RequestBody @Valid EmployeRequest employeRequest){
        return ResponseEntity.ok(userService.updateEmployee(employeRequest));
    }

    @GetMapping(value = "/employee/{employeeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployee(@PathVariable @NotNull String employeeId){
        return ResponseEntity.ok(userService.getEmployee(employeeId));
    }

    @DeleteMapping(value = "/employee/{employeeId}")
    public ResponseEntity<Boolean> deleteEmployee(@PathVariable @NotNull String employeeId){
        return ResponseEntity.ok(userService.deleteEmployee(employeeId));
    }

    @GetMapping(value = "/employees", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> getAllEmployee(){
        return ResponseEntity.ok(userService.getAllEmployee());
    }

    @PostMapping(value = "/customer", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> createCustomer(@RequestBody @Valid CustomerRequest customerRequest){
        return ResponseEntity.ok(userService.createCustomer(customerRequest));
    }

    @PutMapping(value = "/customer", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> updateCustomer(@RequestBody @Valid CustomerRequest customerRequest){
        return ResponseEntity.ok(userService.updateCustomer(customerRequest));
    }

    @GetMapping(value = "/customer/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Customer> getCustomer(@PathVariable @NotNull String customerId){
        return ResponseEntity.ok(userService.getCustomer(customerId));
    }

    @DeleteMapping(value = "/customer/{customerId}")
    public ResponseEntity<Boolean> deleteCustomer(@PathVariable @NotNull String customerId){
        return ResponseEntity.ok(userService.deleteCustomer(customerId));
    }

    @CrossOrigin
    @GetMapping(value = "/customers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Customer>> getAllCustomers(){
        return ResponseEntity.ok(userService.getAllCustomers());
    }
}
