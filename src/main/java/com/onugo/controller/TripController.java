package com.onugo.controller;

import com.onugo.model.document.TripStatus;
import com.onugo.model.request.BookTripRequest;
import com.onugo.model.request.UpdateTripStatusForEmployeeRequest;
import com.onugo.model.response.BookTripResponse;
import com.onugo.model.request.AcceptTripRequest;
import com.onugo.model.response.CustomerBookedTripResponse;
import com.onugo.model.document.TripActive;
import com.onugo.model.document.TripHistory;
import com.onugo.model.document.TripPending;
import com.onugo.service.TripService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin
public class TripController {
    private final TripService tripService;

    @PostMapping(value = "/bookTrip", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookTripResponse> bookTrip(@RequestBody  @Valid final BookTripRequest bookTripRequest){
        return ResponseEntity.ok(tripService.bookTrip(bookTripRequest));
    }

    @PostMapping(value = "/cancelTrip/{tripId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> cancelTrip(@RequestBody final String tripId){
        return ResponseEntity.ok(tripService.cancelTrip(tripId));
    }

    @PostMapping(value = "/acceptTrip", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> acceptTrip(@RequestBody @Valid final AcceptTripRequest acceptTripRequest){
        return ResponseEntity.ok(tripService.acceptTrip(acceptTripRequest));
    }

    @GetMapping(value = "/getCustomerBookedTrip/{tripId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerBookedTripResponse> getCustomerBookedTrip(@PathVariable @NotNull String tripId){
        return ResponseEntity.ok(tripService.getCustomerBookedTrip(tripId));
    }

    @GetMapping(value = "/getAllPendingTrips", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TripPending>> getAllPendingTrips(){
        return ResponseEntity.ok(tripService.getAllPendingTrips());
    }

    @GetMapping(value = "/getCustomerTripsHistory/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TripHistory>> getCustomerTripsHistory(@PathVariable @NotNull String id){
        return ResponseEntity.ok(tripService.getCustomerTripsHistory(id));
    }

    @GetMapping(value = "/getEmployeeTripsHistory/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TripHistory>> getEmployeeTripsHistory(@PathVariable @NotNull String id){
        return ResponseEntity.ok(tripService.getEmployeeTripsHistory(id));
    }

    @PostMapping(value = "/updateTripStatusForEmployee", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> updateEmployeeTripStatus(@RequestBody @Valid final UpdateTripStatusForEmployeeRequest updateTripStatusForEmployeeRequest){
        return ResponseEntity.ok(tripService.updateEmployeeTripStatus(updateTripStatusForEmployeeRequest));
    }

    @GetMapping(value = "/getAllActiveTrips/{tripStatus}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TripActive> > getAllActiveTrips(@PathVariable @NotNull final TripStatus tripStatus){
        return ResponseEntity.ok(tripService.getAllActiveTrips(tripStatus));
    }

    @GetMapping(value = "/tripHistory/{tripId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TripHistory> getTripHistory(@PathVariable @NotNull final String tripId){
        return ResponseEntity.ok(tripService.getTripHistory(tripId));
    }

    @GetMapping(value = "/tripActive/{tripId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TripActive> getTripActive(@PathVariable @NotNull final String tripId){
        return ResponseEntity.ok(tripService.getTripActive(tripId));
    }

    @GetMapping(value = "/tripPending/{tripId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TripPending> getTripPending(@PathVariable @NotNull final String tripId){
        return ResponseEntity.ok(tripService.getTripPending(tripId));
    }
}