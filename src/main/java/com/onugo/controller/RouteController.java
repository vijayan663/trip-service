package com.onugo.controller;

import com.onugo.model.document.Route;
import com.onugo.model.request.RouteRequest;
import com.onugo.service.RouteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@CrossOrigin
public class RouteController {
    private final RouteService routeService;

    @PostMapping(value = "/route", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Route> createRoute(@RequestBody @NotNull RouteRequest routeRequest){
        return ResponseEntity.ok(routeService.createRoute(routeRequest));
    }

    @PutMapping(value = "/route", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> updateRoute(@RequestBody @NotNull RouteRequest routeRequest){
        return ResponseEntity.ok(routeService.updateRoute(routeRequest));
    }

    @DeleteMapping(value = "/route/{routeId}")
    public ResponseEntity<Boolean> deleteRoute(@PathVariable @NotNull String routeId){
        return ResponseEntity.ok(routeService.deleteRoute(routeId));
    }

    @GetMapping(value = "/route/id/{routeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Route> getRouteById(@PathVariable @NotNull String routeId){
        return ResponseEntity.ok(routeService.getRoute(routeId));
    }

    @GetMapping(value = "/route/address/{address}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Route> getRouteByAddress(@PathVariable  @NotNull String address){
        return ResponseEntity.ok(routeService.getRouteByAddress(address));
    }

    @PostMapping(value = "/routes", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Route>> createRoutes(@RequestBody @NotNull List<Route> routes){
        return ResponseEntity.ok(routeService.createRoutes(routes));
    }

    @GetMapping(value = "/routes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Route>> getAllRoutes(){
        return ResponseEntity.ok(routeService.getAllRoutes());
    }
}
