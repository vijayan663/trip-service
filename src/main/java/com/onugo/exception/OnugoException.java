package com.onugo.exception;

import com.onugo.constants.ErrorCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnugoException extends RuntimeException {
    private final ErrorCode errorCode;

    public OnugoException(ErrorCode errorCode){
        super(errorCode.getErrorMessage());
        this.errorCode = errorCode;
    }
}
